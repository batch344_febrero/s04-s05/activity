import java.util.ArrayList;

public class Phonebook {

    private ArrayList<Contact> contacts;


    public Phonebook() {
        contacts = new ArrayList<>();
    }




    public ArrayList<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }


    public void addContact(Contact contact) {
        contacts.add(contact);
    }


    public void displayContacts() {
        if (contacts.isEmpty()) {
            System.out.println("Phonebook is empty.");
        } else {
            for (Contact contact : contacts) {
                System.out.println(contact.getName());
                System.out.println("---------------");
                System.out.println(contact.getName() + " has the following registered numbers:");
                System.out.println(contact.getContactNumber());
                System.out.println("--------------------------");
                System.out.println(contact.getName() + " has the following registered adresses:");
                System.out.println(contact.getAddress());
                System.out.println("===========================");
            }
        }
    }
}
