public class Main {
    public static void main(String[] args) {

        Phonebook phonebook = new Phonebook();


        Contact contact1 = new Contact("John Doe", "+639152468596", "my home in quezon city");
        Contact contact2 = new Contact("Jane Doe", "+639162148573", "my home in Caloocan city");


        phonebook.addContact(contact1);
        phonebook.addContact(contact2);


        phonebook.displayContacts();
    }
}
